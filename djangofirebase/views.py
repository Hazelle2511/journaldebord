
import time
from oauth2client.client import ID_TOKEN_VERIFICATON_CERTS
import pyrebase 
from django.shortcuts import render
#logout
from django.contrib import auth
import datetime
from django.http import HttpResponse



config =  {
  "apiKey": "AIzaSyBQyYYyxGv_-bDipEzKfF_CDnrUMgCEgi8",

  "authDomain": "journaldebord-ca799.firebaseapp.com",

  "databaseURL": "https://journaldebord-ca799-default-rtdb.europe-west1.firebasedatabase.app",

  "projectId": "journaldebord-ca799",

  "storageBucket": "journaldebord-ca799.appspot.com",

  "messagingSenderId": "324936726674",

  "appId": "1:324936726674:web:bb84a5ea3029bc46b0ce4b"

}


firebase = pyrebase.initialize_app(config)
authe = firebase.auth()
db = firebase.database()

def signIn(request):
    return render(request, "signIn.html")

def postSign(request):

    #authentification
    email = request.POST.get("email")
    passw = request.POST.get("pass")

    try:
        user = authe.sign_in_with_email_and_password(email,passw)
    except:
        msg = "invalid credentials"
        return render(request,"signIn.html",{"msg":msg})

    # saving user to session
    # request.session["sess_user"] = user
    session_id = user['idToken']
    request.session['uid'] = str(session_id)
    # retrieve messages from Firebase
    # messages = getFirebaseMessages()

    # return render(request, "welcome.html", {"e":user["email"],"m":messages})
    return render(request, "welcome.html", {"email": user["email"]})

def logOut(request):
    try:
        del request.session['uid']
    except KeyError:
        pass
    return render(request, "signIn.html")

def create(request):
    return render(request, "create.html")

def post_create(request):
    import time
    from datetime import datetime
    time_now=datetime.now()
    millis = int(time.mktime(time_now.timetuple()))
    print('MILLIS ' + str(millis))
    author = request.POST.get("author")
    note = request.POST.get("note")
    receiver = request.POST.get("receiver")

    try: 
        idtoken = request.session['uid']
        a = authe.get_account_info(idtoken)
        a = a['users']
        a = a[0]
        a = a['localId']
        print("INFO "+str(a))
        data = {
            "author": author,
            "note": note,
            "receiver": receiver
        }
        db.child('users').child(a).child('notes').child(millis).set(data,idtoken)
        note = db.child('users').child(a).child('notes').child(millis).get(idtoken).val()
        print("NOTE" + str(note))
        return render(request, "welcome.html")
    except:
        msg = "You are logged Out, Please signIn again to continue"
        return render(request,"signIn.html",{"msg":msg})

def check(request):
      if request.method == 'GET' and 'csrfmiddlewaretoken' in request.GET:
         search = request.GET.get('search')
       
         search = search.lower()
         idtoken = request.session['uid']
         uid = request.GET.get('uid')
         print('SEARCH'+ str(search))
         print(uid)
         timestamps = db.child('users').child(uid).child('notes').shallow().get(idtoken).val()
         note_id=[]  
         for i in timestamps:
            notes = db.child('users').child(uid).child('notes').child(i).child('note').get(idtoken).val()
            notes = str(notes)+"$"+str(i)
            note_id.append(notes)
        # #  return HttpResponse('got it')
        #  timestamps = db.child('users').child(uid).child('notes').shallow().get(idtoken).val()   
        # #  print(timestamps)
        #  note_id=[] 
        #  for i in timestamps:
        #     notes = db.child('users').child(uid).child('notes').child(i).child('note').get(idtoken).val()
        #     notes = str(notes)+"$"+str(i)
        #     note_id.append(notes)

         matching = [str(string) for string in note_id  if search in string.lower()]

         s_note =[]
         s_id =[]
         
         for i in matching:
             note, id = i.split('$')
             s_note.append(note)
             s_id.append(id)

         date=[]
        #  import datetime
         for i in s_id:
             i = float(i)
             dat = datetime.datetime.fromtimestamp(i).strftime('%H:%M %d-%m-%Y')
             date.append(dat)

         comb_list = zip(s_id,date,s_note)
         return render(request, 'check.html',{'comb_list':comb_list, 'uid':uid})

      else:   
         idtoken = request.session['uid']
         a = authe.get_account_info(idtoken)
         a = a['users']
         a = a[0]
         a = a['localId']
         timestamps = db.child('users').child(a).child('notes').shallow().get(idtoken).val()   
         time_list = []
         for i in timestamps:   
             time_list.append(i)   
         time_list.sort(reverse=True)
         print('Timestamps ' + str(timestamps))
         print('Time_List ' + str(time_list))   
         note = []
         for i in time_list:   
             notes=db.child('users').child(a).child('notes').child(i).child('note').get(idtoken).val()
             note.append(notes)
         print('Notes' + str(note))   
         date=[]
         for i in time_list:
             i = float(i)
             dat = datetime.datetime.fromtimestamp(i).strftime('%H:%M %d-%m-%Y')
             date.append(dat)
         print("Date " + str(date))
         

         comb_list = zip(time_list,date,note)
         return render(request, 'check.html',{'comb_list':comb_list, 'uid':a})
# def postMessage(request):
#     data = {
#         "message": request.POST.get("message")
#     }

#     # retrieve user from session
#     user = request.session["sess_user"]

#     # refresh token
#     auth.refresh(user['refreshToken'])

#     # Pass the user's idToken to the push method
#     results = db.child("messages").push(data, user["idToken"])

#     # retrieve messages from Firebase
#     messages = getFirebaseMessages()

#     return render(request, "welcome.html", {"e":user["email"],"m":messages})



# def getFirebaseMessages():
#     """
#     Because Pyrebase generate a list of objects, and each
#     object must be called with .val(), we can not pass it directly
#     to the views (because the functions no more exists).
#     So we must build a simple list of values...
#     """
#     list_messages = {}
#     messages = db.child("messages").get()

#     for m in messages.each():
#         list_messages[m.key()] = m.val()["message"]

#     return list_messages